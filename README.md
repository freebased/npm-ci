# npm-ci

## DevOps Task

The goal of this task is to prepare a generic Gitlab CI pipeline that can be used to lint, test, build and publish a Node.js + TypeScript package to a private npm registry.

We have provided a sample package.json file of the package that you will be preparing the Gitlab CI pipeline for.

### Pipeline Stages

1. Stages that should run only in pipelines created for open Merge Requests in Gitlab:
    - Verify version - should check if current package version (denoted in package.json file) is newer than version that is currently published to the repository
    - Test - should run test npm script
    - Lint - should run lint npm script
    - Build - should run build npm script
2. Stages that should run only in pipelines created after merging feature branch to main branch:
    - Publish - should run build npm script and publish the package to private npm registry

### Expected end results of this task
- Valid gitlab-ci.yml file that is generic - i.e. could be used in other repositories containing other packages without any changes required.
- Explanation of what needs to be set up in repository’s configuration
- Explanation of the functionalities of Gitlab CI that have been used and what purpose do they serve

Testing provided solution is not viable and therefore we are not aiming for working gitlab-ci.yml file as there is no way to fully replicate the environment. We are interested in seeing your approach to the problem.

### Additional information
- Assume private registry is running at https://extra-npm.com 
- Publishing packages to registry is available only after logging in with credentials:
    - username: user
    - password: pass
- Credentials for registry should not be kept in plain text
- Take note of required Node.js version for the package

### Bonus task
Please explain how you would set up this Gitlab CI configuration for a group of repositories where the contents of the gitlab-ci.yml file are not duplicated in every repository - i.e. there is a Single Source of Truth for Gitlab CI configuration. Describe the setup in detail, including information about particular Gitlab CI functionalities that would be used.

## Bonus task solution
To make a generic gitlab-ci file certain requirements needs to be met:
- templates could be used - create separate repo for all jobs you wish to have, store them inside that repo (could be a repo used for infrastructure management - your choice) and include them in every app repo you'd like to - it's worth looking at https://docs.gitlab.com/ee/ci/yaml/includes.html
- nothing is hardcoded, everything is based on variables that are assigned to repository independently
That's mostly it, keep it templated, don't keep anything hardcoded and genericity should be preserved.
